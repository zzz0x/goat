package sound

import (
	"path/filepath"
	"log"
	"os"
	"github.com/aerth/playwav"
)

func Play() {
	dir, err := filepath.Abs(filepath.Dir(os.Args[0]))

	if err != nil {
		log.Fatal(err)
	}

	playwav.FromFile(dir+"/media/play.wav")
}

