package main

import (
	"fmt"
	"os"
	"time"
	"goat/example/sound"
	"goat/example/graphics"
)

func main() {
    go sound.Play()

    time.Sleep(100 * time.Millisecond)
	fmt.Print("Loading")
	for i := 0; i < 21; i++ {
		time.Sleep(500 * time.Millisecond)
		fmt.Print(".")
	}
	fmt.Println()

	go graphics.Show()

	time.Sleep(53 * time.Second)
	os.Exit(0)
}

