package graphics

import (
	"path/filepath"
	"log"
	"time"
	"os"
	"os/exec"
)

func Show() {
	dir, err := filepath.Abs(filepath.Dir(os.Args[0]))

	if err != nil {
		log.Fatal(err)
	}

	cmd := exec.Command("animate", dir+"/media/face.gif")

	time.AfterFunc(51*time.Second, func() { cmd.Process.Kill() })

	cmd.Run()
}

