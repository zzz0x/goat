#!/usr/bin/env bash

ROOT="${PWD}"
CONFIG="${ROOT}/goat.conf"
VENDOR="${ROOT}/vendor"
CACHE="${HOME}/.goat"

main()
{
    case "$1" in
    "update")
        update
        ;;
    "build")
        build "${@:2}"
        ;;
    "cache-clear")
        cache_clear "${@:2}"
        ;;
    *)
        echo "Usage: ./goat.sh [command] [arguments]"
        echo
        echo "commands:"
        echo "    update                     -- (re)install dependencies declared in config"
        echo "    build [arguments]          -- build project passing given arguments to 'go build'"
        echo "    cache-clear [repositories] -- remove all or only listed repositories from source cache"
    esac
}

update()
{
    echo "* Reading config ${CONFIG} ..."
    source "${CONFIG}"

    echo "* Cleaning directory ${VENDOR} ..."
    if ! rm -rf "${VENDOR}"; then
        echo "ERROR: unable to clean directory ${VENDOR}"
        exit 1
    fi

    echo "* Processing dependencies ..."

    for package in "${!dependencies[@]}"
    do
        dependency="${dependencies[$package]}"

        repository="${dependency%::*}"
        commit="${dependency##*::}"

        echo "** found dependency: ${package}"
        echo "        repository = ${repository}"
        echo "        commit     = ${commit}"
        
        target="${VENDOR}/src/${package}"
        mkdir -p "${target}"

        repository_name="$(basename ${repository})"

        cache_dir=`echo "${repository}" | base64 --wrap=0`
        cache_path="${CACHE}/${cache_dir}"
        cache_repo="${cache_path}/${repository_name}"

        echo "*** trying to find source in cache ${cache_path} ..."

        if [ -d "${cache_repo}" ]; then
            echo "*** source found, updating from origin ..."
            echo

            cd "${cache_repo}"
            if ! git fetch --all; then
                echo
                echo "WARNING: fail to update source from origin"
            fi
            cd "${ROOT}"

            echo
            echo "*** source updating completed"
        else
            echo "*** source not found, cloning into cache ..."
            echo

            mkdir -p "${cache_path}"
            if ! git clone --bare "${repository}" "${cache_repo}"; then
                echo
                echo "ERROR: fail to clone repository from origin"
                rm -rf "${cache_path}"
                exit 1
            fi

            echo
            echo "*** source cloned into cache"
        fi

        echo "*** cloning source from cache into vendor directory ${target} ..."
        echo

        if ! git clone "${cache_repo}" "${target}"; then
            echo
            echo "ERROR: fail to clone repository from cache"
            exit 1
        fi
        cd "${target}"

        git remote rename origin cache
        git remote add origin "${repository}"

        echo
        echo "*** checking out version ${commit} ..."
        echo

        if ! git checkout "${commit}"; then
            echo
            echo "ERROR: fail to checkout version ${commit}"
            cd "${ROOT}"
            exit 1
        fi
        cd "${ROOT}"

        echo
        echo "*** dependency has been processed"
    done
    
    echo "* Done."
}

build()
{
    echo "* Reading config ${CONFIG} ..."
    source "${CONFIG}"
    
    src="${ROOT}/src"
    symlink_path="${src}/${root_package}"
    symlink_dir="$(dirname ${symlink_path})"
    symlink_name="$(basename ${symlink_path})"
    
    mkdir -p "${symlink_dir}"
    cd "${symlink_dir}"
    ln -s "${ROOT}" "./${symlink_name}"
    cd "${ROOT}"
    
    echo "* Building ..."
    echo

    export GOPATH="${ROOT}:${ROOT}/vendor"
    go build "$@"

    result=$?
    if ! [ ${result} -eq 0 ]; then
        echo
        echo "ERROR: 'go build' exited with status ${result}"
    fi
    
    rm -rf "${src}"
    exit ${result}
}

cache_clear()
{
    echo "* Clearing the cache"

    if [ $# -eq 0 ]; then
        echo "** removing all repositories from ${CACHE} ..."

        rm -rf ${CACHE}/*
    else
        for repository in "$@"
        do
            cache_dir=`echo "${repository}" | base64 --wrap=0`
            cache_path="${CACHE}/${cache_dir}"

            echo "** removing repository ${cache_path} ..."

            rm -rf "${cache_path}"
        done
    fi

    echo "* Done."
}

main "$@"

