GOAT
====

Great package manager for Go.

Usage
----

0. Forget about GOPATH
1. Create your project anywhere
2. Copy file `goat.sh` into the project root
3. Create file `goat.conf` (see example in this repo)
4. Run `./goat.sh update` to (re)install your dependencies into `./vendor`
5. Run `./goat.sh build [options for "go build"]` to build the binary

Features
----

* Ability to build project anywhere, avoiding bothering with GOPATH
* You can specify commit, branch or tag for dependencies you use
* Simple and clean workflow, no need to mess with Makefile and symlinking

**+BONUS:**

* You can add any dependencies, even those that can't be go-get'ed (e.g. "evilcorporation/doomsday/machine" hosted on Github)
* No external dependencies, everything you need is `bash` and `git`
* Your dependencies are cached inside `~/.goat` to speed-up further updating

Example
----

```sh
sudo apt-get install libasound2-dev imagemagick # needed to build an example
git clone https://bitbucket.org/zzz0x/goat.git
cd goat
./goat.sh update
./goat.sh build -v -buildmode=pie -o ./example goat/example
./example # you may put on your headphones
```

Help
----

Run `./goat.sh` without arguments to get full list of available commands.

Credits
----

**Music/FX**: [Bobby Prince](http://www.vgmpf.com/Wiki/index.php?title=Bobby_Prince) credited as Robert Prince

**GIF**: ID SOFTWARE

License
----

The source code is licensed under MIT license. See [LICENSE](LICENSE).

